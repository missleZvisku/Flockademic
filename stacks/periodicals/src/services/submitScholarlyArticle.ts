import { Account } from '../../../../lib/interfaces/Account';
import { Database } from '../../../../lib/lambda/middleware/withDatabase';

// This file is ignored for test coverage in the Jest configuration
// since it is merely a translation of Javascript objects to SQL queries.
export async function submitScholarlyArticle(
  database: Database,
  articleIdentifier: string,
  periodicalIdentifier: string,
  account: Account,
): Promise<{
  datePublished: string;
  identifier: string;
  isPartOf: { identifier: string; };
}> {
  interface PartOfRow {
    scholarly_article: string;
    is_part_of: string;
    date_published: Date;
  }

  const result: [ undefined, PartOfRow ] = await database.tx((t) => {
    const queries = [
      // Make sure the current account is owner of the periodical
      t.none(
        // tslint:disable-next-line:no-invalid-template-strings
        'INSERT INTO scholarly_article_authors (scholarly_article, author) VALUES (${articleId}, ${authorId})'
          + ' ON CONFLICT (scholarly_article, author) DO NOTHING',
        { articleId: articleIdentifier, authorId: account.identifier },
      ),
      // Publish the periodical
      t.one(
        'INSERT INTO scholarly_articles_part_of (scholarly_article, is_part_of, date_published)'
          // tslint:disable-next-line:no-invalid-template-strings
          + ' SELECT ${articleId}, uuid, NOW() FROM periodicals WHERE identifier=${periodicalId}'
          + ' ON CONFLICT (scholarly_article, is_part_of) DO UPDATE SET date_published=EXCLUDED.date_published'
          + ' RETURNING scholarly_article, is_part_of, date_published',
        { articleId: articleIdentifier, periodicalId: periodicalIdentifier },
      ),
    ];

    return t.batch(queries);
  });

  return {
    datePublished: result[1].date_published.toISOString(),
    identifier: result[1].scholarly_article,
    isPartOf: {
      identifier: result[1].is_part_of,
    },
  };
}
