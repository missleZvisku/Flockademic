import {
  PostGenerateUploadUrlForPeriodicalAssetsRequest,
  PostGenerateUploadUrlForPeriodicalAssetsResponse,
} from '../../../../lib/interfaces/endpoints/periodical';
import { Request } from '../../../../lib/lambda/faas';
import { getUploadUrl } from '../../../../lib/lambda/getUploadUrl';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { SessionContext } from '../../../../lib/lambda/middleware/withSession';
import { addHeaderImageToPeriodical } from '../services/addHeaderImageToPeriodical';
import { canManagePeriodical } from '../utils/canManagePeriodical';

export async function generateAssetUploadUrl(
  context: Request<PostGenerateUploadUrlForPeriodicalAssetsRequest> & DbContext & SessionContext,
): Promise<PostGenerateUploadUrlForPeriodicalAssetsResponse> {
  if (context.session instanceof Error) {
    throw new Error('You do not appear to be logged in.');
  }

  if (typeof process.env.asset_upload_bucket === 'undefined') {
    // tslint:disable-next-line:no-console
    console.log('No upload bucket defined.');
    throw new Error('There was a problem uploading your image.');
  }

  if (!context.body || !context.body.targetCollection || !context.body.targetCollection.identifier
    || !context.body.result || !context.body.result.image) {
    throw(new Error('No journal for which to upload an image specified.'));
  }
  const periodicalId = context.body.targetCollection.identifier;

  const allowedToEdit = await canManagePeriodical(context.database, periodicalId, context.session);
  if (!allowedToEdit) {
    throw new Error('You can only set the header image of journals you manage.');
  }

  const filename = context.body.result.image;

  const bucketObjectName = `${periodicalId}_header_${filename}`;
  const uploadBucket = (process.env as any).asset_upload_bucket;
  const uploadUrl = getUploadUrl(uploadBucket, bucketObjectName);
  const downloadUrl =
    `https://s3.${(process.env as any).aws_region}.amazonaws.com/${uploadBucket}/`
    + encodeURIComponent(bucketObjectName);

  await addHeaderImageToPeriodical(
    context.database,
    context.session,
    periodicalId,
    downloadUrl,
  );

  return {
    object: {
      toLocation: uploadUrl,
    },
    result: {
      image: downloadUrl,
  } };
}
